# Setup the AWS provider | provider.tf

# variable aws_access_key {}
# variable aws_secret_key {}


terraform {
  required_version = ">= 0.12"
  
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 2.70.1" #GitLab does not seem to permit version upgrade with gitlab-terraform init -upgrade
      }

  }

  backend "http" {}

}

provider "aws" {
# version     = "~> 2.12"  # DEPRECATED
# region      = var.aws_region
profile = "default"
region = "us-east-1"
}
