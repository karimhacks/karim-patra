# Application Definition
app_name        = "karimpatra" # Do NOT enter any spaces *OR* dashes!
app_environment = "test" # Dev, Test, Prod, etc

#AWS authentication variables
# aws_access_key    = "{$aws_access_key}"
aws_access_key    = "{$var.AWS_ACCESS_KEY_ID}"
# aws_secret_key    = "{$var.AWS_SECRET_ACCESS_KEY}"
aws_secret_key    = "{$aws_secret_key}"
aws_key_pair_name = "karim-patra"
aws_key_pair_file = "karim-patra.pem"
aws_region        = "us-east-1" # Danger is my middle name, baby!

# Application access
app_sources_cidr   = ["0.0.0.0/0"] # Specify a list of IPv4 IPs/CIDRs which can access app load balancers
admin_sources_cidr = ["0.0.0.0/0"] # Specify a list of IPv4 IPs/CIDRs which can admin instances
